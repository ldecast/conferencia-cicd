const express = require('express');
const cors = require('cors');

/* init */
const PORT = 8081;
const app = express();

/* Middlewares */
app.use(express.json());
app.use(cors());

/* Endpoint principal */
app.get('/', (req, res) => {
    res.send('RESPUESTA DESDE EL SERVICIO 2.');
});

/* Start */
app.listen(PORT, () => {
    console.log(`Servicio 2 corriendo en el puerto ${PORT}.`)
});