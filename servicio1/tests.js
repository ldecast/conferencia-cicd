function sleep(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms * 1000));
}

async function test() {
    await sleep(1);
    console.log("Prueba 1 completa ✅");
    await sleep(2);
    console.log("Prueba 2 completa ✅");
    await sleep(2);
    console.log("Prueba 3 completa ✅");
    await sleep(1);
}

test();