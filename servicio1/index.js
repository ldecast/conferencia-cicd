const express = require('express');
const cors = require('cors');

/* init */
const PORT = 8080;
const app = express();

/* Middlewares */
app.use(express.json());
app.use(cors());

/* Endpoint principal */
app.get('/', (req, res) => {
    res.send('MODIFIQUÉ EL SERVICIO 1');
});

/* Start */
app.listen(PORT, () => {
    console.log(`Servicio 1 corriendo en el puerto ${PORT}.`)
});